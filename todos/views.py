from django.shortcuts import redirect, render, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


# Create your views here.
def show_todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def show_todo_item(request, id):
    todo_item = get_object_or_404(TodoList, id=id)
    context = {
        "todo_item": todo_item,
    }
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            list = form.save()
            # list.user = request.user
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def update_todo(request, id):
    todo_item = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_item)
        if form.is_valid():
            list = form.save()
            # redirect back to detail view of the model
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm(instance=todo_item)

    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_todos(request, id):
    todo_item = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_item.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            item = form.save()
            # list.user = request.user
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/add_item.html", context)


def update_todo_item(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            # redirect back to detail view of the model
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    context = {
        "todo_item": todo_item,
        "form": form,
    }
    return render(request, "todos/update_item.html", context)
